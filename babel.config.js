module.exports = function (api) {
  const cfg = {
    presets: [
      [
        '@babel/preset-env', {
          targets: {
            node: '12'
          }
        }
      ],
      '@babel/preset-react',
      '@babel/preset-flow'
    ],
    plugins: []
  };

  api.cache(false);

  return cfg;
};
