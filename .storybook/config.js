import { configure, addDecorator, addParameters } from '@storybook/react';
import React from 'react';
import backgrounds from './storyutils/backgrounds';

const req = require.context('../src', true, /stories\.js$/);

function loadStories() {
  req.keys().forEach(filename => req(filename));
}

addDecorator(
    story => (
        React.createElement(
            'div',
            {
                style: {
                    padding: '2rem',
                    textAlign: 'center'
                }
            },
            story()
        )
    )
);
addParameters(backgrounds);
configure(loadStories, module);
