import React from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { mainReducer } from '../../src/store';

export function reduxDecorator (story) {
    return React.createElement(
        Provider,
        {
            store: createStore(mainReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())
        },
        story()
    )
}