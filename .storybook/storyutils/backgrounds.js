export default {
    backgrounds: [
        { name: 'light', value: '#fff', default: true },
        { name: 'dark', value: '#333' }
    ]
}
