const { resolve } = require('path');
const mainConfig = require('../webpack.config.js');

module.exports = ({ config }) => {
    const newConfig = Object.assign({}, config);
    const setInConfig = (key, value) => { newConfig[key] = value };

    mainConfig.module.rules.forEach(
        value => { newConfig.module.rules.push(value) }
    );

    Object.entries(mainConfig.resolve.alias).forEach(
        ([key, value]) => { newConfig.resolve.alias[key] = value }
    );

    newConfig.resolve.alias['$storyutils'] = resolve(__dirname, 'storyutils');

    return newConfig;
}
