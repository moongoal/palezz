// @flow

import * as actionType from './actionTypes';

type Action = {
    type: string,
    payload: any
};

/**
 * Set a new palette.
 *
 * @param {Array} palette The new palette to set
 */
export const setPalette = (palette: Array<any>): Action => ({
  type: actionType.SET_PALETTE,
  payload: { palette }
});

/**
 * Append a color to the palette.
 *
 * @param {*} color The color to append to the palette
 */
export const addPaletteColor = (color: any): Action => ({
  type: actionType.ADD_PALETTE_COLOR,
  payload: { color }
});

/**
 * Replace a color in the palette with a new one.
 *
 * @param {Integer} index Index of the palette color to change
 * @param {*} newColor New color to set within the palette
 */
export const changePaletteColor = (index: number, newColor: any): Action => ({
  type: actionType.CHANGE_PALETTE_COLOR,
  payload: {
    index,
    newColor
  }
});

/**
 * Remove a color from the palette.
 *
 * @param {Integer} index Index of the color to remove
 */
export const removePaletteColor = (index: number): Action => ({
  type: actionType.REMOVE_PALETTE_COLOR,
  payload: { index }
});
