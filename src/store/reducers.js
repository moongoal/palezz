// @flow
import type { Color, Action } from '$types';
import * as actionType from './actionTypes';

/**
 * Palette reducer.
 *
 * @param {Array} state The palette of colors
 * @param {Object} action The action
 */
export function palette (state: Array<Color> = [], action: Action) {
  const { type, payload } = action;

  switch (type) {
    case actionType.SET_PALETTE:
      return [...payload.palette];

    case actionType.ADD_PALETTE_COLOR:
      return [...state, payload.color];

    case actionType.CHANGE_PALETTE_COLOR: {
      const { index, newColor } = payload;
      const newState = [...state];

      newState.splice(index, 1, newColor);

      return newState;
    };

    case actionType.REMOVE_PALETTE_COLOR: {
      const { index } = payload;
      const newState = [...state];

      newState.splice(index, 1);

      return newState;
    };

    default:
      return state;
  }
}
