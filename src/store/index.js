import { combineReducers } from 'redux';
import * as reducers from './reducers';

export * from './actions';

/**
 * The main application reducer.
 */
export const mainReducer = combineReducers(reducers);
