export const SET_PALETTE = 'palette.set';
export const ADD_PALETTE_COLOR = 'palette.add';
export const REMOVE_PALETTE_COLOR = 'palette.remove';
export const CHANGE_PALETTE_COLOR = 'palette.modify';
