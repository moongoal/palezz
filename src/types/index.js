// @flow
/**
 * Useful types collection.
 *
 * @module types
 */

/**
 * A vanilla event callback function.
 *
 * @callback EventCallback
 * @param {Event} [event] The event object
 */
export type EventCallback = (event?: Class<Event>) => mixed;

/**
 * Numeric callback function.
 *
 * @callback NumberCallback
 * @param {number} newValue
 */
export type NumberCallback = (n: number) => mixed;

type HSLColor = { h: number, s: number, l: number, a?: number };
type RGBColor = { r: number, g: number, b: number, a?: number };
export type Color =
    | string
    | HSLColor
    | RGBColor
    | [ number, number, number ];

export type Action = {
    type: string,
    payload: any
};

/**
 * HSL-color-related callback function.
 *
 * @callback HSLCallback
 * @param {Object} c HSL color
 */
export type HSLCallback = (c: HSLColor) => mixed;
