import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';
import './index.html';

var appContainer = document.getElementById('app');
var app = <App />;

ReactDOM.render(app, appContainer);
