// @flow
import type { EventCallback, Color } from '$types';
import React from 'react';
import classNames from 'classnames';
import color from 'color';
import './index.scss';
import svgCloseIcon from '$svg/x.svg';

type ControlBarProps = {| onDelete: EventCallback |};

type SwatchProps = {
    color: Color,
    className?: string,
    primary?: boolean,
    selected?: boolean,
    style?: Object,
    onDelete: EventCallback,
    onClick: EventCallback
};

/**
 * Create a control bar component.
 *
 * @param {Object} props Props object
 * @param {@module:types.callback:EventCallback} onDelete Delete callback
 */
function ControlBar ({ onDelete }: ControlBarProps) {
  const onDeleteCallback = e => {
    onDelete();
    e.stopPropagation();
  };

  return (
    <div className='swatch-delete'>
      <img className="swatch-delete-icon" src={ svgCloseIcon } title="Delete" alt="Delete" onClick={ onDeleteCallback } />
    </div>
  );
}

/**
 * Create a swatch component.
 *
 * @param {Object} props Props object
 * @param {Object|string} props.color Color to show in the swatch
 * @param {boolean} [props.primary=false] True if the swatch is the primary of its row
 * @param {boolean} [props.selected=false] True if the swatch is currently selected
 * @param {EventCallback} [props.onDelete] Called when the delete button is clicked
 * @param {EventCallback} [props.onClick] Called when the body of the swatch is clicked
 * @param {...*} [props.other] Extra props directly passed to the underlying DOM element
 */
export function Swatch ({ color: propColor, className, primary, selected, style = {}, onDelete, onClick, ...otherProps } : SwatchProps) {
  const colorName: string = color(propColor).hex();
  const swatchProps = {
    ...otherProps,
    className: classNames(
      'swatch',
      primary ? 'primary' : null,
      selected ? 'selected' : null,
      className
    ),
    style: {
      ...style,
      backgroundColor: colorName
    },
    onClick
  };
  const controlBar = primary ? <ControlBar onDelete={ onDelete } /> : null;

  return (
    <div { ...swatchProps }>
      { controlBar }
      <div className="spacer" />
      <div className="swatch-internal">
        <div className="swatch-label" onClick={ e => e.stopPropagation() }>
          { colorName }
        </div>
      </div>
    </div>
  );
}
