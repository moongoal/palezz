import React from 'react';
import { storiesOf } from '@storybook/react';
import { actions } from '@storybook/addon-actions';
import { withKnobs, color, boolean } from '@storybook/addon-knobs';
import { Swatch } from '.';

export const swatchActions = actions({
  onClick: 'clicked',
  onDelete: 'deleted'
});

storiesOf('Color Selection|Swatch Book/Swatch', module)
  .addDecorator(withKnobs)
  .add('default', () => <Swatch color={color('Color', '#d60e0e')} selected={boolean('Selected', false)} {...swatchActions} />)
  .add('primary', () => <Swatch color={color('Color', '#d60e0e')} primary="true" selected={boolean('Selected', false)} {...swatchActions} />);
