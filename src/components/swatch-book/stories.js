import React from 'react';
import { storiesOf } from '@storybook/react';
import { actions } from '@storybook/addon-actions';
import { withKnobs, number, array } from '@storybook/addon-knobs';
import { SwatchBook } from '.';

const exampleColors = ['red', 'green', 'blue', 'cyan'];
const bookActions = actions({
  onSelect: 'swatch selected'
});

export const defaultProps = () => ({
  shades: number('Shades', 3, { range: true, min: 1, max: 10 }),
  colors: array('Colors', exampleColors),
  step: number('Lightness steps', 0.25, { range: true, min: 0.01, max: 1, step: 0.1 }),
  selected: number('Selected', -1)
});

storiesOf('Color Selection|Swatch Book', module)
  .addDecorator(withKnobs)
  .addDecorator(story => <div style={{ width: number('Width', 800, { range: true, min: 100, max: 1800 }) + 'px', margin: 'auto' }}>{story()}</div>)
  .add('default', () => <SwatchBook {...defaultProps()} {...bookActions} />);
