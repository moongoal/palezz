// @flow
import type { Color, NumberCallback } from '$types';
import React from 'react';
import classNames from 'classnames';
import color from 'color';
import { Swatch } from '$components/swatch';
import './index.scss';

export const MAX_SHADES = 5;

type MakeSwatchesOptions = {
    onSelect?: NumberCallback,
    onDelete?: NumberCallback,
    colorIndex: number,
    selected: boolean
};

type SwatchBookProps = {
    className?: string,
    shades?: number,
    colors?: any, // Array<Color>, HACK: Set to `any` because flow still doesn't support Array.prototype.flat()
    step?: number,
    onSelect?: NumberCallback,
    onDelete?: NumberCallback,
    selected?: number
};

/**
 * Create a sequence of shade swatches for a given primary color.
 *
 * @param {Object} swatchColor Primary swatch color
 * @param {number} nShades Number of shades (and tints) to create in a row
 * @param {number} lightnessStep Lightness step ratio
 * @param {Object} options Additional options
 * @param {module:types.callback:NumberCallback} [options.onSelect] Selection callback
 * @param {module:types.callback:NumberCallback} [options.onDelete] Deletion callback
 * @param {number} [options.colorIndex] Index of color in the palette
 * @param {selected} [options.selected] True if the swatch row sequence selected
 */
function makeSwatches (
  swatchColor: Color,
  nShades: number,
  lightnessStep: number,
  options: MakeSwatchesOptions = {}
) {
  const shades = [0];
  const {
    onSelect,
    onDelete,
    colorIndex,
    selected
  } = options;

  for (let i = 1; i <= nShades; i++) {
    const shadeLightnessAbs = i * lightnessStep;

    shades.push(shadeLightnessAbs);
    shades.unshift(-shadeLightnessAbs);
  }

  return shades.map((lightnessFactor, idx) => {
    const c: string = color(swatchColor).lighten(lightnessFactor).hex();
    const props = {
      key: (colorIndex.toString(): string) + idx + c,
      color: c,
      primary: lightnessFactor === 0,
      selected,
      onClick: undefined,
      onDelete: undefined
    };

    if (onSelect) { props.onClick = () => onSelect(colorIndex) }

    if (onDelete) { props.onDelete = () => onDelete(colorIndex) }

    return <Swatch {...props} />;
  });
}

/**
 * Create a swatch book.
 *
 * @param {Object} props Component props
 * @param {string} [props.className] Component class
 * @param {number} [props.shades] Number of shades per color to display
 * @param {*[]} [props.colors] Color palette to present
 * @param {number} [props.step] Lightness step for creating the shades
 * @param {module:types.callback:NumberCallback} [props.onSelect] Swatch selection callback
 * @param {module:types.callback:NumberCallback} [props.onDelete] Swatch selection callback
 * @param {number} [props.selected] Index of currently selected palette color
 */
export function SwatchBook ({ className, shades = 3, colors = [], step = 0.25, onSelect, onDelete, selected }: SwatchBookProps) {
  shades = Math.min(shades, MAX_SHADES);

  const gridAttrs = {
    className: classNames('swatch-book', className),
    style: {
      gridTemplateColumns: 'auto '.repeat(2 * shades + 1).trimEnd()
    }
  };
  const swatches = colors.map(
    (c, i) => makeSwatches(
      c,
      shades,
      step,
      {
        onSelect,
        onDelete,
        colorIndex: i,
        selected: i === selected
      }
    )).flat();

  return <div {...gridAttrs}>{swatches}</div>;
}
