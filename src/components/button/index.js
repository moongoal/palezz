import React from 'react';
import classNames from 'classnames';
import './index.scss';

/**
 * Create a new Button component.
 *
 * @param {Object} [props] The button props
 * @param {boolean} [props.primary] True if the button is meant for a primary action
 * @param {...*} [props.other] Extra props directly passed to the underlying DOM element
 */
export function Button (props) {
  const { children, className, primary, ...restProps } = props;
  const buttonAttrs = {
    className: classNames({
      button: true,
      primary,
      [className]: className
    }),
    ...restProps
  };

  return <div {...buttonAttrs}>{children}</div>;
}
