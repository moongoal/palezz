import React from 'react';
import { storiesOf } from '@storybook/react';
import { actions } from '@storybook/addon-actions';
import { withKnobs, text, boolean } from '@storybook/addon-knobs';
import { Button } from '.';
import svgBlackWhite from '$svg/black-white.svg';

export const defaultActions = actions({
  onClick: 'click'
});

export const commonProps = () => ({
  primary: boolean('Primary', false)
});

storiesOf('Basic|Button', module)
  .addDecorator(withKnobs)
  .add('text only',
    () => <Button {...defaultActions} {...commonProps()} >{text('Content', 'Hi, I\'m a button!')}</Button>,
    { knobs: { escapeHTML: false } }
  )
  .add('icon only', () => (
    <Button {...defaultActions} {...commonProps()} title="Black or White" >
      <img style={{ verticalAlign: 'middle' }} src={svgBlackWhite} />
    </Button>
  ))
  .add('icon and text', () => (
    <Button {...defaultActions} {...commonProps()} title="Black or White">
      <img style={{ height: '1rem', verticalAlign: '-1px' }} src={svgBlackWhite} />
      <span style={{ marginLeft: '.25rem' }}>{text('Caption', 'Black or White')}</span>
    </Button>
  ),
  { knobs: { escapeHTML: false } }
  );
