// @flow
import React from 'react';
import { mount } from 'enzyme';
import { Button } from '.';

describe('Button', function () {
  it('should support the onClick event', function (done) {
    const clbk = () => done();
    const c = mount(<Button onClick={clbk} />);

    c.simulate('click');
  });
});
