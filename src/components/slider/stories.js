import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import { actions } from '@storybook/addon-actions';
import { withKnobs, number } from '@storybook/addon-knobs';
import { Slider } from '.';

const containerStyle = () => ({
  width: number('Width', 400, { range: true, min: 0, max: 800 }) + 'px'
});
export const eventHandlers = actions({
  onChange: 'Value changed'
});
export const props = () => ({
  value: number('Value', 0, { range: true, min: 0, max: 1, step: 0.01 })
});

function InteractiveSlider ({ initialValue }) {
  const [value, setValue] = useState(initialValue);
  const onChange = newValue => setValue(newValue);

  return <Slider { ...{ value, onChange } } />;
}

storiesOf('Basic|Slider', module)
  .addDecorator(withKnobs)
  .addDecorator(story => <div style={containerStyle()}>{story()}</div>)
  .add('default', () => <Slider {...eventHandlers} {...props()} />)
  .add('interactive', () => <InteractiveSlider />);
