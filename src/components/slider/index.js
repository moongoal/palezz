// @flow
import * as React from 'react';
import classNames from 'classnames';
import './index.scss';

const { useEffect, useState, createRef } = React;

type Props = {
    className?: string,
    onChange?: (number) => any,
    value?: number
};

const SLIDER_WIDTH = 100;
const SLIDER_HANDLE_GRIP_THICKNESS = 0.75;

/**
 * Create a slider component.
 *
 * @param {Object} props Component props
 * @param {string} [props.className] Component class
 * @param {module:types.callback:NumberCallback} [props.onChange] Slider value change callback
 * @param {number} [props.value=0.] Slider value - must be a floating point value between 0 and 1.
 */
export function Slider (props: Props) {
  const [aspectRatio, setAspectRatio] = useState(0.1);
  const { className: propClasses, onChange, value = 0.0 } = props;

  if (value < 0 || value > 1) { throw new Error('Invalid value ' + value) }

  const sliderHeight = SLIDER_WIDTH * aspectRatio;
  const sliderHandleHeight = sliderHeight;
  const sliderHandleWidth = sliderHandleHeight;
  const sliderHandleGripHeight = sliderHandleHeight * 0.75;
  const svgProps = {
    className: classNames('slider', propClasses)
  };
  const viewBox = `0 0 ${SLIDER_WIDTH} ${sliderHeight}`;
  const [isDragging, setDragging] = useState(false);
  const handleBackgroundRef = createRef();
  const svgRef = createRef();
  const backgroundRef = createRef();
  const recomputePcFromEvent = event => {
    if (backgroundRef.current) {
      const tgtRect = backgroundRef.current.getBoundingClientRect();

      if (handleBackgroundRef.current) {
        const handleWidth = handleBackgroundRef.current.getBoundingClientRect().width;
        const newValue = Math.round(Math.min(1, Math.max(0, (event.clientX - tgtRect.left - handleWidth / 2) / (tgtRect.width - handleWidth))) * 100) / 100;

        if (newValue !== value && onChange) {
          onChange(newValue);
        }
      }
    }
  };

  const onMouseDown = event => { recomputePcFromEvent(event); setDragging(true) };
  const onMouseMove = event => { isDragging && recomputePcFromEvent(event) };
  const onMouseUp = event => { if (isDragging) { recomputePcFromEvent(event); setDragging(false) } };
  const handleLeft = value * (SLIDER_WIDTH - sliderHandleWidth);
  const handleTop = 0;

  useEffect(() => {
    const svgComponent = svgRef.current;

    if (svgComponent) {
      const svgClientRect = svgComponent.getBoundingClientRect();
      const svgAspectRatio = svgClientRect.height / svgClientRect.width;

      if (svgAspectRatio !== aspectRatio) { setAspectRatio(svgAspectRatio) }
    }
  });

  return (
    <svg version="1.1" viewBox={ viewBox } { ...svgProps } onMouseMove={ onMouseMove } onMouseUp={ onMouseUp } ref={ svgRef }>
      <g onMouseDown={ onMouseDown }>
        <rect className="border" width={ SLIDER_WIDTH } height={ sliderHeight } ref={ backgroundRef } />
        <g className="handle" transform={ `translate(${handleLeft}, ${handleTop})` }>
          <rect width={ sliderHandleWidth } height={ sliderHandleHeight } ref={ handleBackgroundRef } />
          <rect
            className="grip"
            width={ SLIDER_HANDLE_GRIP_THICKNESS }
            height={ sliderHandleGripHeight }
            x={ (sliderHeight - SLIDER_HANDLE_GRIP_THICKNESS) / 2 }
            y={ (sliderHeight - sliderHandleGripHeight) / 2 }
          />
          <rect
            className="grip"
            width={ SLIDER_HANDLE_GRIP_THICKNESS }
            height={ sliderHandleGripHeight }
            x={ (sliderHeight / 2 - SLIDER_HANDLE_GRIP_THICKNESS) / 2 }
            y={ (sliderHeight - sliderHandleGripHeight) / 2 }
          />
          <rect
            className="grip"
            width={ SLIDER_HANDLE_GRIP_THICKNESS }
            height={ sliderHandleGripHeight }
            x={ (sliderHeight * 1.5 - SLIDER_HANDLE_GRIP_THICKNESS) / 2 }
            y={ (sliderHeight - sliderHandleGripHeight) / 2 }
          />
        </g>
      </g>
    </svg>
  );
}
