import { storiesOf } from '@storybook/react';
import { actions } from '@storybook/addon-actions';
import { withKnobs, number } from '@storybook/addon-knobs';
import React from 'react';
import { ColorWheel } from '.';

export const callbacks = actions({
  onColorSelect: 'Color selected',
  onColorHover: 'Color hovered'
});

export const props = () => ({
  hues: number('Hues', 12, { range: true, min: 1, max: 32 }),
  shades: number('Shades', 4, { range: true, min: 1, max: 16 })
});

storiesOf('Color Selection|Color Wheel', module)
  .addDecorator(withKnobs)
  .addDecorator(story => <div style={{ width: '400px' }}>{story()}</div>)
  .add('light', () => <ColorWheel {...props()} {...callbacks} />)
  .add('dark', () => <ColorWheel {...props()} {...callbacks} dark="true" />);
