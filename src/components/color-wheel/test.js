// @flow
import React from 'react';
import { mount } from 'enzyme';
import { ColorWheel } from './index.js';

describe('Color-Wheel', function () {
  describe('events', function () {
    it('should support the onColorSelect event', function (done) {
      const clbk = () => done();
      const c = mount(<ColorWheel onColorSelect={clbk} />);

      c.find('.tint-container').first().simulate('click');
    });

    it('should support the onColorHover event', function (done) {
      const clbk = () => done();
      const c = mount(<ColorWheel onColorHover={clbk} />);

      c.find('.tint-container').first().simulate('mouseover');
    });
  });
});
