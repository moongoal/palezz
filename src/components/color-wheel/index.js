// @flow
import React from 'react';
import color from 'color';
import classNames from 'classnames';
import './index.scss';

/**
 * Color event callback function.
 *
 * @callback ColorEventCallback
 * @param {Object} color The HSL color from the event
 * @param {number} color.h Hue
 * @param {number} color.s Saturation
 * @param {number} color.l Lightness
 */

type Props = {
    hues?: number,
    shades?: number,
    className?: string,
    onColorSelect?: (color: Object) => mixed,
    onColorHover?: (color: Object) => mixed,
    dark?: boolean
};

const rad = Math.PI / 180; // Deg -> rad conversion factor
const SLICE_MARGIN_ANGLE = 1 * rad;
const SLICE_MARGIN_LENGTH = 1;
const WHEEL_CENTRE = { x: 50, y: 50 };
const WHEEL_RADIUS = 50;
const WHEEL_SHADOW_COLOR = '#bbb7';

/**
 * Return an array of hues uniformly distributed across 0-360.
 *
 * @param {number} nHues Number of hues to return
*/
function createHues (nHues: number): Array<number> {
  return [...new Array(nHues).keys()].map(x => x * 360 / nHues);
}

/**
 * Create a slice of shades for a given hue.
 *
 * @param {number} hue The hue to base the element on
 * @param {number} nShades The number of shades to produce
 * @param {number} sliceBeginAngle The angle of the beginning of the slice
 * @param {number} sliceEndAngle The angle of the end of the slice
 * @param {Props} ownProps The properties of the component
 */
function createLightnessSlice (
  hue: number,
  nShades: number,
  sliceBeginAngle: number,
  sliceEndAngle: number,
  ownProps: Props
) {
  // Unit vectors
  const darkFactor = (-1) ** (ownProps.dark ? 1 : 0);
  const p1u = { x: Math.cos(sliceBeginAngle), y: Math.sin(sliceBeginAngle) };
  const p2u = { x: Math.cos(sliceEndAngle), y: Math.sin(sliceEndAngle) };
  const sliceExternalLength = WHEEL_RADIUS / nShades; // Length including margin
  const sliceInternalLength = sliceExternalLength - 2 * SLICE_MARGIN_LENGTH; // Actual length of slice, margin excluded
  const { onColorSelect, onColorHover } = ownProps;

  return [...new Array(nShades).keys()].map(i => {
    // Radii
    const externalRadius = WHEEL_RADIUS - sliceExternalLength * i - SLICE_MARGIN_LENGTH;
    const internalRadius = externalRadius - sliceInternalLength + SLICE_MARGIN_LENGTH;

    // Upper points
    const p1 = { x: WHEEL_CENTRE.x + p1u.x * externalRadius, y: WHEEL_CENTRE.y + p1u.y * externalRadius };
    const p2 = { x: WHEEL_CENTRE.x + p2u.x * externalRadius, y: WHEEL_CENTRE.y + p2u.y * externalRadius };

    // Lower points
    const p3 = { x: WHEEL_CENTRE.x + p1u.x * internalRadius, y: WHEEL_CENTRE.y + p1u.y * internalRadius };
    const p4 = { x: WHEEL_CENTRE.x + p2u.x * internalRadius, y: WHEEL_CENTRE.y + p2u.y * internalRadius };

    const pathCommands = [
            `M ${p1.x} ${p1.y}`,
            `L ${p3.x} ${p3.y}`,
            `A ${internalRadius} ${internalRadius} 0 0 1 ${p4.x} ${p4.y}`,
            `L ${p2.x} ${p2.y}`,
            `A ${externalRadius} ${externalRadius} 0 0 0 ${p1.x} ${p1.y}`,
            'Z'
    ];
    const hslColor = { h: hue, s: 100, l: 50 * (1 + darkFactor * i / nShades) };
    const c = color(hslColor).hex(); // "#xxxxxxxx"-formatted color
    const selectionHandlerProxy = onColorSelect ? () => onColorSelect(hslColor) : null;
    const hoverHandlerProxy = onColorHover ? () => onColorHover(hslColor) : null;

    return (
      <g key={c + i} onClick={selectionHandlerProxy} onMouseOver={hoverHandlerProxy} fill={c} className="tint-container">
        <path d={pathCommands.join('\n')} fill={WHEEL_SHADOW_COLOR} filter="url(#dropShadow)" />
        <path d={pathCommands.join('\n')} className="tint" />
      </g>
    );
  });
}

/**
 * Create a hue element.
 *
 * @param {number} hue The hue to create the element for
 * @param {number} hueStep The step (in degrees) between one displayed hue and the next
 * @param {number} nShades The numbe rof shades
 * @param {Props} ownProps The component own props
 */
function createHueElement (
  hue: number,
  hueStep: number,
  nShades: number,
  ownProps: Props
) {
  const angle = hue * rad;
  const halfAngleStep = hueStep * rad / 2;
  const sliceBeginAngle = angle - halfAngleStep + SLICE_MARGIN_ANGLE;
  const sliceEndAngle = angle + halfAngleStep - SLICE_MARGIN_ANGLE;
  const slices = createLightnessSlice(hue, nShades, sliceBeginAngle, sliceEndAngle, ownProps);

  return (
    <g key={hue}>
      {slices}
    </g>
  );
}

/**
 * Create the `defs` section of the SVG component.
 */
function createDefs () {
  return (
    <defs key="defs">
      <filter id="dropShadow">
        <feOffset in="SourceGraphic" dx=".8" dy=".8" />
        <feGaussianBlur in="FillPaint" stdDeviation=".3 .3" />
      </filter>
      <filter id="blur">
        <feGaussianBlur in="FillPaint" stdDeviation=".5 .5" />
      </filter>
    </defs>
  );
}

/**
 * Create a color wheel component.
 *
 * @param {Props} [props] The component props
 * @param {number} [props.hues=12] Number of hues to display around the wheel
 * @param {number} [props.shades=4] Number of shades to display
 * @param {ColorEventCallback} [props.onColorSelect] Color select event callback
 * @param {ColorEventCallback} [props.onColorHover] Color hover event callback
 * @param {boolean} [props.dark=false] True if dark tones should be displayed instead of light ones
 * @param {...*} [props.other] Extra props directly passed to the underlying DOM element
 */
export function ColorWheel (props: Props) {
  const { hues: nHues = 12, shades: nShades = 4, className, dark, onColorSelect, onColorHover, ...restProps } = props;
  const hueStep = 360.0 / nHues;
  const hues = createHues(nHues);
  const svgChildren = hues.map(h => createHueElement(h, hueStep, nShades, props));
  const svgClassName = classNames(className, 'color-wheel');

  svgChildren.unshift(createDefs());

  return <svg {...restProps} className={svgClassName} viewBox="0 0 100 100" version="1.1">{svgChildren}</svg>;
}
