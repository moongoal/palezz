import React from 'react';
import { ColorChooser } from '$components/color-chooser';

/**
 * Main application constructor.
 */
export default function () {
  return (
    <ColorChooser />
  );
}
