// @flow
import type { EventCallback } from '$types';
import React from 'react';
import { Button } from '$components/button';
import svgBlackWhite from '$svg/black-white.svg';
import { switchUsageMode } from './utils';

type Props = {
    darkMode: boolean,
    onSwitchDarkMode: EventCallback,
    usageMode: string,
    onSwitchUsageModeButtonClicked: EventCallback
};

export function OptionsBar ({ darkMode, onSwitchDarkMode, usageMode, onSwitchUsageModeButtonClicked }: Props) {
  const darkModeLabel = `Show ${darkMode ? 'Tints' : 'Shades'}`;

  return (
    <div className="option-buttons-container">
      <Button title={darkModeLabel} onClick={onSwitchDarkMode}>
        <img src={svgBlackWhite} alt="Light/Dark" style={{ width: '16px', marginRight: '8px', verticalAlign: '-2px' }}/>
        {darkModeLabel}
      </Button>
      <Button onClick={onSwitchUsageModeButtonClicked}>Switch to { switchUsageMode(usageMode) }</Button>
    </div>
  );
}
