// @flow
import type { Color, HSLCallback } from '$types';
import React from 'react';
import color from 'color';
import { Slider } from '$components/slider';

type Props = {|
    palette: Array<Color>,
    selectedColorIndex: number,
    onHSLChange: HSLCallback
|};

/**
 * Create a controls pane used in the palette editor when in advanced mode.
 *
 * @param {Object} props Component props
 */
export function AdvancedModeControlsPane ({ palette, selectedColorIndex, onHSLChange }: Props) {
  let selectedColor: any = color('black').hsl();

  if (!onHSLChange) { onHSLChange = () => {} }

  if (selectedColorIndex >= 0) { selectedColor = palette[selectedColorIndex] } else if (palette.length) { selectedColor = palette[palette.length - 1] }

  selectedColor = color(selectedColor);

  const onChangeHue = value => onHSLChange(selectedColor.hue(value * 360));
  const onChangeSat = value => onHSLChange(selectedColor.saturationl(value * 100));
  const onChangeLig = value => onHSLChange(selectedColor.lightness(value * 100));

  return (
    <div className="advanced-mode-controls-container">
      <div><label>Hue</label><Slider onChange={onChangeHue} value={selectedColor.hue() / 360} /></div>
      <div><label>Saturation</label><Slider onChange={onChangeSat} value={selectedColor.saturationl() / 100} /></div>
      <div><label>Lightness</label><Slider onChange={onChangeLig} value={selectedColor.lightness() / 100} /></div>
    </div>
  );
}
