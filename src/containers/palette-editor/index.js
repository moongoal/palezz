// @flow
import type { Color } from '$types';
import React, { useState } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import color from 'color';
import {
  addPaletteColor,
  changePaletteColor,
  removePaletteColor
} from '$store';
import { ColorWheel } from '$components/color-wheel';
import { SwatchBook } from '$components/swatch-book';
import { AdvancedModeControlsPane } from './adv-pane';
import { OptionsBar } from './options-bar';
import {
  MODE_SIMPLE,
  MODE_ADVANCED,
  switchUsageMode
} from './utils';
import './index.scss';

type Props = {
    className?: string,
    palette?: Array<Color>,
    addPaletteColor: (c: Color) => mixed,
    changePaletteColor: (i: number, c: Color) => mixed,
    removePaletteColor: (i: number) => mixed
};

/**
 * Create a palette editor component.
 *
 * @param {Object} props Component props
 * @param {Object} [props.className] Component class
 * @param {*[]} [props.palette] Palette of colors to edit
 * @param {function} [props.addPaletteColor] Callback function called when a color is added
 * @param {function} [props.changePaletteColor] Callback function called when a color is modified
 * @param {function} [props.removePaletteColor] Callback function called when a color is removed
 */
function _PaletteEditor (props: Props) {
  const {
    className: propClassName,
    palette = [],
    addPaletteColor,
    changePaletteColor,
    removePaletteColor
  } = props;
  const [selectedColorIndex, setSelectedColorIndex] = useState(-1);
  const [darkMode, setDarkMode] = useState(false);
  const [usageMode, setUsageMode] = useState(MODE_SIMPLE);
  const className = classNames(
    'palette-editor',
    propClassName
  );
  const onColorSelectClbk = cIndex => {
    setSelectedColorIndex(cIndex === selectedColorIndex ? -1 : cIndex);
  };
  const onColorDeleteClbk = cIndex => {
    removePaletteColor(cIndex);

    if (selectedColorIndex === cIndex) {
      setSelectedColorIndex(-1);
    } else if (selectedColorIndex > cIndex) {
      setSelectedColorIndex(selectedColorIndex - 1);
    }
  };
  const onSwitchDarkMode = () => setDarkMode(!darkMode);
  const onColorWheelSelectColor = c => {
    const clr = color(c).hsl();

    if (selectedColorIndex !== -1) { changePaletteColor(selectedColorIndex, clr) } else { addPaletteColor(clr) }
  };
  const onHSLChange = newColor => {
    if (selectedColorIndex >= 0) { changePaletteColor(selectedColorIndex, newColor) }
  };
  const onSwitchUsageModeButtonClicked = () => setUsageMode(switchUsageMode(usageMode));
  const advancedModeControls = (usageMode === MODE_ADVANCED) ? <AdvancedModeControlsPane {...{ palette, selectedColorIndex, onHSLChange }} /> : null;

  return (
    <div className={className}>
      <div className="color-wheel-container">
        <ColorWheel dark={darkMode} onColorSelect={onColorWheelSelectColor}/>
        <OptionsBar {...{ darkMode, onSwitchDarkMode, usageMode, onSwitchUsageModeButtonClicked }} />
        {advancedModeControls}
      </div>
      <div className="swatch-book-container">
        <SwatchBook colors={palette} onSelect={onColorSelectClbk} onDelete={onColorDeleteClbk} selected={selectedColorIndex} />
      </div>
    </div>
  );
}

const mapStateToProps = ({ palette }) => ({
  palette
});

const mapDispatchToProps = {
  addPaletteColor,
  changePaletteColor,
  removePaletteColor
};

export const PaletteEditor = connect(mapStateToProps, mapDispatchToProps)(_PaletteEditor);
