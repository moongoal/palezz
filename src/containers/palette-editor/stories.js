import React from 'react';
import { storiesOf } from '@storybook/react';
import { reduxDecorator } from '$storyutils/redux';
import { PaletteEditor } from '.';

const containerProps = {
  style: {
    margin: 'auto'
  }
};

storiesOf('Color Selection|Palette Editor', module)
  .addDecorator(reduxDecorator)
  .addDecorator(story => <div {...containerProps}>{story()}</div>)
  .add('default', () => <PaletteEditor />);
