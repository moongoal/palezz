// @flow
export const MODE_SIMPLE = 'simple';
export const MODE_ADVANCED = 'advanced';

/**
 * Switches between simple and advanced mode.
 *
 * @param {string} mode Mode to switch
 * @returns The switched mode
 */
export function switchUsageMode (mode: string): string {
  switch (mode) {
    case MODE_SIMPLE:
      return MODE_ADVANCED;
    case MODE_ADVANCED:
      return MODE_SIMPLE;
    default:
      throw new Error(`Invalid mode "${mode}"`);
  }
}
