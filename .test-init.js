const { addHook } = require('pirates');
const { JSDOM } = require('jsdom');
const Enzyme = require('enzyme');
const Adapter = require('enzyme-adapter-react-16');

// Ignore imports
const IGNORE_EXTENSIONS = [
    '.scss'
];

// Fake DOM
global.dom = new JSDOM('');
global.window = dom.window;
global.document = window.document;
global.navigator = window.navigator;

// Enzyme
Enzyme.configure({
    adapter: new Adapter()
});

addHook(
    (code, filename) => '',
    { exts: IGNORE_EXTENSIONS }
);
