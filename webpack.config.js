const { resolve } = require('path');
const { HotModuleReplacementPlugin } = require('webpack');

module.exports = {
  mode: 'development',
  module: {
    rules: [
      { // Babel
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              '@babel/preset-env',
              '@babel/preset-react'
            ]
          }
        }
      },
      { // HTML
        test: /\.html$/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[name].[ext]'
          }
        }
      },
      { // SCSS
        test: /\.scss$/,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader'
        ],
        include: resolve(__dirname, 'src')
      }
    ]
  },
  resolve: {
    alias: {
      $theme: resolve(__dirname, 'src', 'theme'),
      $components: resolve(__dirname, 'src', 'components'),
      $containers: resolve(__dirname, 'src', 'containers'),
      $svg: resolve(__dirname, 'src', 'svg'),
      $store: resolve(__dirname, 'src', 'store'),
      $types: resolve(__dirname, 'src', 'types')
    }
  },
  plugins: [
    new HotModuleReplacementPlugin()
  ],
  devServer: {
    hot: true
  }
};
